## Internship

### Packages

These packages need to be installed:
 * make
 * texlivefull (minted)
 * xelatex

### Compile tex

To compile the project you can simplify do:
```sh
  $ make
```

To clean the repository, you can do:
```sh
 $ make clean
```
